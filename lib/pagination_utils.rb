# frozen_string_literal: true

require 'pagination_utils/version'
require 'pagination_utils/railtie'

require_relative '../app/controllers/concerns/paginate'

module PaginationUtils
end

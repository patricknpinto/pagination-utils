# frozen_string_literal: true

module Paginate
  extend ActiveSupport::Concern

  private

  def paginate(collection)
    return collection if pagination_disabled?

    collection = collection.page(page).per(per_page)

    assign_headers(collection)

    collection
  end

  # rubocop:disable Metrics/AbcSize
  def assign_headers(collection)
    headers['Access-Control-Expose-Headers'] = accepted_headers
    headers['X-Total'] = collection.total_count.to_s
    headers['X-Total-Pages'] = collection.total_pages.to_s
    headers['X-Page'] = collection.current_page.to_s
    headers['X-Per-Page'] = collection.limit_value.to_s
    headers['X-Next-Page'] = collection.next_page.to_s
    headers['X-Prev-Page'] = collection.prev_page.to_s
  end
  # rubocop:enable Metrics/AbcSize

  def per_page
    return 25 unless params[:per_page]
    return 25 if params[:per_page].to_i.zero?

    params[:per_page].to_i > 100 ? 100 : params[:per_page]
  end

  def page
    return 1 unless params[:page]
    return 1 if params[:page].to_i.zero?

    params[:page]
  end

  def accepted_headers
    'X-Total,X-Total-Pages,X-Page,X-Per-Page,X-Next-Page,X-Prev-Page'
  end

  def pagination_disabled?
    params[:page] == 'all'
  end
end
